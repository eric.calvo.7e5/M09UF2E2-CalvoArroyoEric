using System;
using System.Threading;

namespace M09UF2E2_CalvoArroyoEric
{
    class Program
    {
        static void Main()
        {
            Exercici1();
            //Exercici2();
        }

        static void Exercici1()
        {
            Thread t1 = new Thread(() => ImprimirFrase("Una vegada hi havia un gat"));
            Thread t2 = new Thread(() => ImprimirFrase("En un lugar de la Mancha"));
            Thread t3 = new Thread(() => ImprimirFrase("Once upon a time in the west"));

            t1.Start();
            t1.Join();
            t2.Start();
            t2.Join();
            t3.Start();
            t3.Join();

            Console.WriteLine("Presiona cualquier tecla para salir...");
            Console.ReadKey();
        }

        static void ImprimirFrase(string frase)
        {
            string[] palabras = frase.Split(' ');

            foreach (string palabra in palabras)
            {
                Console.Write(palabra + " ");
                Thread.Sleep(500);
            }

            Console.WriteLine();
        }

        static void Exercici2()
        {
            Nevera nevera = new Nevera(6);

            Thread anittaThread = new Thread(() => {
                nevera.Llenar("Anitta");
            });
            Thread badBunnyThread = new Thread(() => {
                nevera.Beber("Bad Bunny");
            });
            Thread lilNasXThread = new Thread(() => {
                nevera.Beber("Lil Nas X");
            });
            Thread manuelTurizoThread = new Thread(() => {
                nevera.Beber("Manuel Turizo");
            });

            anittaThread.Start();
            anittaThread.Join();
            badBunnyThread.Start();
            badBunnyThread.Join();
            lilNasXThread.Start();
            lilNasXThread.Join();
            manuelTurizoThread.Start();
            manuelTurizoThread.Join();

            Console.WriteLine("Fin del programa");
        }
    }
    class Nevera
    {
        private int numCervezas;

        public Nevera(int cervezasIniciales)
        {
            numCervezas = cervezasIniciales;
        }

        public void Llenar(string persona)
        {
            int cervezasNuevas = new Random().Next(7);
            Console.WriteLine("{0} está llenando la nevera con {1} cervezas nuevas", persona, cervezasNuevas);
            numCervezas = Math.Min(numCervezas + cervezasNuevas, 9);
            Console.WriteLine("La nevera ahora tiene {0} cervezas", numCervezas);
        }

        public void Beber(string persona)
        {
            int cervezasABeber = new Random().Next(Math.Min(numCervezas, 7));
            Console.WriteLine("{0} está bebiendo {1} cervezas de la nevera", persona, cervezasABeber);
            numCervezas = Math.Max(numCervezas - cervezasABeber, 0);
            Console.WriteLine("La nevera ahora tiene {0} cervezas", numCervezas);
        }
    }
}
